/**
 * convert an integer number to a roman numeral string
 * @param {integet} num
 * @returns {string}
 */
export const toRoman = (num) => {
  if (typeof num !== 'number') 
  return false; 
  
  const digits = String(+num).split("");
  const key = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM",
    "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC",
    "","I","II","III","IV","V","VI","VII","VIII","IX"];
  let roman_num = "";
  let i = 3;
  while (i--){
    roman_num = (key[+digits.pop() + (i * 10)] || "") + roman_num;
  }
  return Array(+digits.join("") + 1).join("M") + roman_num;
}

const charToInt = (character) => {
  switch (character){
    case 'I': return 1;
    case 'V': return 5;
    case 'X': return 10;
    case 'L': return 50;
    case 'C': return 100;
    case 'D': return 500;
    case 'M': return 1000;
    default: return -1;
  }
}

/**
 * convert roman numeral string to an integer number
 * @param {string} romanNum 
 * @returns {integer}
 */
export const fromRoman = (romanNum) => {
  if (romanNum === null) return false;
  const romanArray = romanNum.split('');
  let num = charToInt(romanArray[0]);
  
  for (let i = 1; i < romanNum.length; i++){
    let curr = charToInt(romanArray[i]);
    let pre = charToInt(romanArray[i-1]);
    if (curr <= pre){
      num += curr;
    } else {
      num = num - pre*2 + curr;
    }
  }
  return num;
}


  

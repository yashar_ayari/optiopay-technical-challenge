import * as romanNumeral from './romanNumerals';

describe('romanNumeral convertor' , () => {
  it('it should convert integer to roman numeral', () => {
    expect(romanNumeral.toRoman(2700)).toBe('MMDCC');
  });

  it('it should convert roman numeral to Integer', () => {
    expect(romanNumeral.fromRoman('XXVI')).toBe(26);
  });
});

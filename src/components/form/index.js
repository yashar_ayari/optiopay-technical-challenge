import React from 'react';
import { Form } from 'react-bootstrap';

const FormComponent = ({
  romanValue,
  integerValue,
  handleChange,
}) => (
  <Form>
    <Form.Group controlId="formBasicEmail">
      <Form.Label>Roman numeral</Form.Label>
      <Form.Control
        type="text"
        value={romanValue}
        onChange={value => handleChange(value.target.value, 'roman')}
      />
    </Form.Group>
    <Form.Group controlId="formBasicPassword">
      <Form.Label>Integer</Form.Label>
      <Form.Control
        type="text"
        value={integerValue}
        onChange={value => handleChange(value.target.value, 'integer')}
      />
    </Form.Group>
  </Form>
);

export default FormComponent;

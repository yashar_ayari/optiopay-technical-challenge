import React from 'react';
import { mount } from 'enzyme';
import App from './index';

describe('Modal component', () => {
  let wrapper;
  it('It should render the app component', () => {
    wrapper = mount(
      <App />
    );
    expect(wrapper.find('header.app-header')).toHaveLength(1);
  });
});

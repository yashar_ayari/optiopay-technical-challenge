import React, { Fragment } from 'react';
import Form from '../form';
import * as romanNumerals from '../../utils/romanNumerals';
import styles from './styles.css';

class App extends React.Component {
  state = {
    romanValue: '',
    integerValue: '',
  }

  handleChange = (value, type) => {
    if (type === 'roman') {
      this.setState({
        romanValue: value,
        integerValue: romanNumerals.fromRoman(value.toUpperCase()),
      });
    } else {
      this.setState({
        romanValue: romanNumerals.toRoman(parseInt(value)),
        integerValue: value,
      });
    }
  }

  render() {
    const { romanValue, integerValue } = this.state;
    return (
      <div className='wrapper'>
        <header className="app-header">
          <h1 className="app-title">test project</h1>
        </header>
        <Form
          romanValue={romanValue}
          integerValue={integerValue}
          handleChange={this.handleChange}
        />
      </div>
    );
  } 
}

export default App;

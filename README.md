# Optiopay technical challenge implementation #

When you run the development server, a form will be open on the default browser that lets you convert an integer to roman numerals and vice versa.

### How do I run the development server? ###

```bash
$ git clone git@bitbucket.org:yashar_ayari/optiopay-technical-challenge.git
$ cd optiopay-technical-challenge
$ npm install
$ npm run start
```

### How do I run unit tests? ###

```bash
$ git clone git@bitbucket.org:yashar_ayari/optiopay-technical-challenge.git
$ cd optiopay-technical-challenge
$ npm install
$ npm run test
```
